class Empanada {
    constructor(sabor, cantidad) {
        this.sabor = sabor
        this.cantidad = cantidad
    }
}

const carneSuave = new Empanada('Carne Suave', 1);
console.log(carneSuave.sabor);

const pedido = [];

function crearPedido(sabor, cantidad) {
    for (let i = 0; i < cantidad; i++) {
        pedido.push(new Empanada(sabor));
    }
}

crearPedido('Carne Picante', 5);
console.log(pedido);
