const precioUnidad = 250;
const descuentoMediaDocena = 0.1;
const descuentoDocena = 0.15;

const mensajeBienvenida = `¡Bienvenido a Empanadas Online!

Precios:
- Por unidad $${precioUnidad}.
- Promo media docena ${descuentoMediaDocena * 100}% off.
- Promo docena ${descuentoDocena * 100}% off.

Elegí a continuación la cantidad y los sabores.`;

const mensajeMenu = `Menu:

1. Carne Suave (CS)    4. Jamón y Queso (JQ)
2. Carne Picante (CP)  5. Roquefort y Jamón (RJ)
3. Verdura (VE)        6. Pollo (PO)

`;





// ----- Inicio del programa -----

alert(mensajeBienvenida);


// ----- Ingreso y verificación de cantidad de empanadas -----

let cantidadDeEmpanadas = Number(prompt('Ingresa la cantidad de empanadas que vas a llevar:'));
let cantidadEsCorrecta = false;

while (cantidadEsCorrecta === false) {
    if (cantidadDeEmpanadas <= 0 || cantidadDeEmpanadas === null) {
        alert('No se ingresó cantidad.');
        cantidadDeEmpanadas = Number(prompt('Ingresa la cantidad de empanadas que vas a llevar:'));
    } else if (isNaN(cantidadDeEmpanadas)) {
        alert('Datos incorrectos.');
        cantidadDeEmpanadas = Number(prompt('Ingresa la cantidad de empanadas que vas a llevar:'));
    } else if (cantidadDeEmpanadas > 60) {
        alert('Para pedidos mayores a 5 docenas comunicate el 0800-TELEFONO-AQUI');
        cantidadDeEmpanadas = Number(prompt('Ingresa la cantidad de empanadas que vas a llevar:'));
    } else {
        cantidadEsCorrecta = true;
    }
}

alert(`Elegí los ${cantidadDeEmpanadas} sabores que vas a llevar.`)
console.log(`El cliente ingresó ${cantidadDeEmpanadas} empanadas.`);


// ----- Proceso de elección de sabores -----

let sabores = `Los sabores elegidos son:`;
let esIngresoCorrecto = false;
let pedidoCompleto = 0;

while (pedidoCompleto < cantidadDeEmpanadas) {
    let saborIngresado = Number(prompt(`${mensajeMenu} Ingresa el número de opción elegida:`));
    let verificarValor = saborIngresado > 0 && saborIngresado <= 6 && saborIngresado !== null && !isNaN(saborIngresado);

    if (verificarValor) {
        if (saborIngresado === 1) {
            sabores += `
            - Carne Suave`;
            pedidoCompleto++;
        } else if (saborIngresado === 2) {
            sabores += `
            - Carne Picante`;
            pedidoCompleto++;
        } else if (saborIngresado === 3) {
            sabores += `
            - Verdura`;
            pedidoCompleto++;
        } else if (saborIngresado === 4) {
            sabores += `
            - Jamón y Queso`;
            pedidoCompleto++;
        } else if (saborIngresado === 5) {
            sabores += `
            - Roquefort y Jamón`;
            pedidoCompleto++;
        } else if (saborIngresado === 6) {
            sabores += `
            - Pollo`;
            pedidoCompleto++;
        } 
    } else {
        alert('Ingreso incorrecto');
    }

    if (pedidoCompleto === cantidadDeEmpanadas) {
        esIngresoCorrecto = true;
        break;
    }
}

alert(`${sabores}

Continua con tu pago.`);
console.log(`Ingreso correcto: ${esIngresoCorrecto}.`);
console.log(sabores);


// ----- Proceso de pago -----

const totalSinDescuentos = cantidadDeEmpanadas * precioUnidad;

function aplicarDescuento(precio, descuento) {
    
}

const determinarDescuento = (cantidad, total) => {
    let totalAPagar;
    let descuento;
    if (cantidad < 6) {
        totalAPagar = total;
    } else if (cantidad >= 6 && cantidad < 12) {
        const descuento = totalSinDescuentos * descuentoMediaDocena;
        totalDelPedido = totalSinDescuentos - descuento;
    } else if (cantidadDeEmpanadas >= 12) {
        const descuento = totalSinDescuentos * descuentoDocena;
        totalDelPedido = totalSinDescuentos - descuento;
    }
    return totalAPagar;
}

let totalDelPedido;

if (cantidadDeEmpanadas < 6) {
    totalDelPedido = totalSinDescuentos;
} else if (cantidadDeEmpanadas >= 6 && cantidadDeEmpanadas < 12) {
    const descuento = totalSinDescuentos * descuentoMediaDocena;
    totalDelPedido = totalSinDescuentos - descuento;
} else if (cantidadDeEmpanadas >= 12) {
    const descuento = totalSinDescuentos * descuentoDocena;
    totalDelPedido = totalSinDescuentos - descuento;
}

alert(`Tu total es $${totalDelPedido}.`);
console.log(`El cliente abonó $${totalDelPedido}.`);


// ----- Fin del programa -----

console.log('Fin del programa.');
